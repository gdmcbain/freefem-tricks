\documentclass{beamer}
\mode<presentation>
\usetheme{Berlin} 

\newcommand{\nhat}{\mbox{\boldmath{$\hat n$}}}

\author[G. D. McBain]{Geordie McBain}
\institute{Institut Jean le Rond d'Alembert} 
\title{Domain decomposition techniques for interfacial discontinuities} 

\begin{document}
\maketitle

\section{Introduction}

\begin{frame}
  \frametitle{Connexion and substructuring}

  \begin{itemize}
  \item Two ways to look at domain decomposition
    \begin{itemize}
    \item Decomposing domains by iterative substructuring
    \item Linking subdomains through interfacial conditions
    \end{itemize}

  \item Small easy generalizations of techniques for the former mean
    they can be used for the latter.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Queries on \texttt{freefempp} mailing list}

  \begin{itemize}
  \item \textbf{Electrical simulation with a voltage jump at an intermediate
    layer} (2011-05-20)

    {\it
      My questions are:

    1) Is it possible with FreeFem++ to apply a constant voltage
      jump at the surface where both cubes are touching and how to do
      it?

      2) If possible, how to insert a resistive 2D sheet at this
      surface (current dependent voltage jump)?    }

  \item \textbf{Non linear heat transfer equation and Thermal Contact Resistance (TCR)} (2012-08-15)

    {\it how could we simulate the Thermal contact resistance between two
      materials?}

  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Steady thermal conduction}
  \begin{itemize}
  \item For definiteness, consider the steady conduction of heat
    \begin{displaymath}
      -\nabla\cdot(\mathsf K \nabla T) = s
    \end{displaymath}
    where
    \begin{description}

    \item[$\mathsf K$] conductivity
    \item[$T$] temperature
    \item[$s$] volumetric rate of generation
    \end{description}
  \item Weak form ($\forall U$):
    \begin{displaymath}
      \left\langle \nabla U, \mathsf K\nabla T\right\rangle
      - \left[U, \nhat\cdot\mathsf K\nabla T\right]
        - \left\langle U, s\right\rangle = 0
    \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Natural interfacial conditions}
  \begin{itemize}
  \item basic interfacial conditions (like Kirchhoff's circuit laws):
      \begin{itemize}
    \item equality of temperature (like potential at node)
    \item zero sum of heat (like current into node)
    \end{itemize}
  \item
    \begin{align*}
      T_1 &= T_2 \\
      \nhat_1\cdot \mathsf K_1\nabla T_1 
      + \nhat_2\cdot \mathsf K_2\nabla T_2 &= 0
    \end{align*}
  \item Often have $\mathsf K_1 \neq \mathsf K_2$ in applications.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Classical engineering heat transfer problem I}
  \begin{quotation}
    Given a two-layer furnace wall of thickness and conductivity $L_i$
    and $K_i$ $(i=1, 2)$, if the inside is at $T_1$ and the outside at
    $T_2$, what is the temperature distribution?
  \end{quotation}

Dirichlet--Dirichlet solutions, given $T_{\mathrm{int}}$:
\begin{align*}
      \text{subdomain solutions: } 
      T_1 - T_{\mathrm{int}} &= L_1 q_1 / K_1 \\
      T_{\mathrm{int}} - T_2 &= L_2 q_2 / K_2 
\end{align*}
      
Poincar\'e--Steklov Dirichlet$\rightarrow$Neumann operator:
\begin{align*}
  q_1 - q_2
      &= \frac{T_1K_1}{L_1} + \frac{T_2K_2}{L_2} - 
      \left(\frac{K_1}{L_1} + \frac{K_2}{L_2}\right) T_{\mathrm{int}}
      \\
      \text{Solution: }
      T_{\mathrm{int}} &= 
      \left(\frac{T_1K_1}{L_1} + \frac{T_2K_2}{L_2}\right) \div
      \left(\frac{K_1}{L_1} + \frac{K_2}{L_2}\right)
    \end{align*}

\end{frame}

\begin{frame}
  \frametitle{Classical engineering heat transfer problem II}
  \begin{quotation}
    Given a two-layer furnace wall of thickness and conductivity $L_i$
    and $K_i$ $(i=1, 2)$, if the inside is at $T_1$ and the outside at
    $T_2$, how much heat $q$ is lost?
  \end{quotation}

  Neumann--Neumann solutions, given $q$:
  \begin{align*}
    \text{subdomain solutions: } 
    T_1 - T_{\mathrm{int}}^{-} &= L_1 q / K_1 \\
    T_{\mathrm{int}}^{+} - T_2 &= L_2 q / K_2 
  \end{align*}
      
  Poincar\'e--Steklov Neumann$\rightarrow$Dirichlet operator:
  \begin{align*}
    T_{\mathrm{int}}^{+} - T_{\mathrm{int}}^{-}
    &= T_2 - T_1 + \left(\frac{L_1}{K_1} + \frac{L_2}{K_2}\right) q
    \\
    \text{Solution: }
    q &= \frac{T_1 - T_2}{\frac{L_1}{K_1} + \frac{L_2}{K_2}}
  \end{align*}

\end{frame}

\begin{frame}
  \frametitle{Neumann--Neumann domain decomposition}
  \begin{itemize}
  \item This idea generalizes to finite element solutions.
  \item Guess the flux at the interface.
  \item Solve Neumann problem on either subdomain.
  \item Compute the temperature mismatch at interface.
  \item Iterate to find the flux eliminating the mismatch.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Generalized interfacial conditions}
  \begin{itemize}
  \item various matching criteria:

  \begin{itemize}
  \item specified jump: $T_1 = T_2 + \Delta T$
  \item nonlinear algebraic: $f (T_1, T_2) = 0$
    \begin{itemize}
    \item e.g. equilibrium chemical partitioning between phases
    \end{itemize}
  \item contact resistance: $T_1 = T_2 + Rq$
  \item general nonlinear: $f (T_1, T_2, q) = 0$
  \end{itemize}

\item Any of these fit the Neumann--Neumann framework.

\item Just `solve' this equation for the interfacial flux.
\item Noting that $T_1 = T_1 (q)$ and $T_2 = T_2 (q)$.
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Generalized interfacial conditions (cont.)}
  \begin{itemize}
  \item Could also have a superficial power source.
  \item Then there's a jump in the flux.
  \item Method should still apply
    \begin{itemize}
    \item guess the flux on one side
    \item calculate the flux on the other from the condition
    \item solve the two Neumann problems
    \item etc.
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Simple example with interfacial resistance}

\begin{frame}
  \frametitle{Example of Carnes \& Copps (2008)}

  \textsc{Carnes, B. R., \& K. D. Copps} (2008) Thermal contact algorithms.
  Sandia Report SAND2008-2607

  \begin{align*}
    \text{D.E.:} \qquad -\nabla^2 T &= \mathop{\mathrm{sgn}} x
    \qquad (-1 < x < 1) \\
\text{Dirichlet conditions:}\qquad T(-1) = 0,
    &\quad T(1) = 1 \\
    \text{flux balance:}\qquad -T'(0^-) &= -T'(0^+) \\
    \text{interfacial resistance:}\qquad
    -RT'(0) &= T(0^-) - T(0^+)    
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Results of Carnes \& Copps (2008)}
  \includegraphics[width=\linewidth]{carnes/drawing.png}
\end{frame}

\begin{frame}
  \frametitle{Reproduction in FreeFem++}
  \centering
  \includegraphics[width=.8\linewidth]{../neumann-neumann/neumann-neumann.png}
\end{frame}

\subsection{Implementation in FreeFem++}

\begin{frame}
  \frametitle{Geometry and mesh}
  \includegraphics[width=\linewidth]{neumann-neumann-mesh.png}

  Solve in two dimensions (to investigate mismatched meshes).
\end{frame}

\begin{frame}
  \frametitle{Implementation in FreeFem++}
  \begin{itemize}
  \item See \S\,9.8.3 Schwarz-gc.edp.
  \item define interfacial flux $q$ on one whole subdomain
    \begin{itemize}
    \item values meaningless off interface
    \end{itemize}
  \item solve Neumann problem for each subdomain
  \item compute interfacial temperature mismatch
  \item iterate using conjugate gradients
  \end{itemize}
\end{frame}

\begin{frame}[containsverbatim]
  \frametitle{Subdomain Neumann problems}
\begin{verbatim}
problem Pb1 (u1, v1, init=i) = 
  int2d (Th1) dot(grad(v1), grad(u1))
  + int2d (Th1) (-v1*s1) 
  + int1d (Th1, inner) (+q*v1) 
  + on (left, u1=0);
problem Pb2 (u2, v2, init=i) = 
  int2d(Th2) dot(grad(v2), grad(u2))
  + int2d (Th2) (-v2*s2) 
  + int1d (Th2, inner) (-q*v2) 
  + on (right, u2=1);
\end{verbatim}
\end{frame}

\begin{frame}[containsverbatim]
  \frametitle{Computing the mismatch $\Delta T - Rq$}

\begin{verbatim}
varf b (u1, v1) = int1d (Th1, inner) (v1 * u1);
matrix B = b (Vh1, Vh1);

func real[int] BoundaryProblem (real[int] &l) { 
   q[] = l; 
   Pb1; Pb2; i++;
   v1 = u1 - u2;		// temperature jump
   real[int] q1 = B * v1[];
   v1 = R * q;		// resistive jump
   real[int] q2 = B * v1[];
   q[] = q1 - q2;
   return q[]; 
};
\end{verbatim}
\end{frame}

\begin{frame}[containsverbatim]
  \frametitle{Finding the correct interfacial flux}
\begin{verbatim}
Vh1 p=0; 
LinearCG (BoundaryProblem, p[]);
BoundaryProblem (p[]);
\end{verbatim}
\end{frame}

\begin{frame}
  \frametitle{Neumann--Neumann method}
  \begin{itemize}
  \item A particular advantage of the Neumann--Neumann method:
    \begin{itemize}
    \item We don't have to compute interfacial fluxes.  Even with an
      interfacial resistance, for which the flux appears in the
      matching condition, we can use the guessed interfacial flux.
  \end{itemize}
\item Having to compute fluxes on each subdomain then transfer to
  where iteration variable was defined would have been fiddly.
\item Thus a Dirichlet--Dirichlet method is less convenient.
  \begin{itemize}
  \item not attempted here
  \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Summary of extension on \S\,9.8.3}

 Instead of just calculating the temperature jump,
 also subtract resistance times the guessed flux.


\end{frame}

\section{New test case}

\begin{frame}
  \frametitle{An axially symmetric problem}
  \begin{itemize}
  \item Instead of Carnes \& Copps (2008) one-dimensional solution
  \item we propose an axially symmetric one.
  \item Still has a simple exact solution
  \item less trivial realization in two-dimensional finite elements
  \item Also tests curved interfaces.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{The axially symmetric problem}
  \begin{align*}
    \text{D.E.:} \qquad
    -\nabla\cdot (K_1 \nabla T_1)
    &= s_1 \qquad (r_0 < r < r_1) \\
    -\nabla\cdot (K_2 \nabla T_2)
    &= s_2 \qquad (r_1 < r < r_2) \\
    \text{Dirichlet conditions: } T (r_0) &= T_0 \\
    T (r_2) &= T_2 \\
    \text{flux balance:}\qquad -T'(r_1^-) &= -T'(r_1^+) \\
    \text{interfacial resistance:}\qquad
    -RT'(r_1) &= T(r_1^-) - T(r_1^+)    
  \end{align*}
\end{frame}

\begin{frame}
  \frametitle{General analytical solution of Neumann problem}
  \begin{itemize}
  \item Dirichlet condition at one radius $T (r_i) = T_i$

  \item Guessed Neumann condition at another $-K_i T'
    (r_{\mathrm{int}}) = q_{\mathrm{int}}$
  \item source $s_i$ and conductivity $K_i$ constant per annular layer
  \item centre excluded: $r_i > 0$
  \end{itemize}

  \begin{align*}
    T (r) &= T_i + 
    \frac{s_i}{2}\left(
      r_{\mathrm{int}}^2\ln\frac{r}{r_i}
      +
      \frac{r_i^2-r^2}{2}
    \right)
    - \frac{r_{\mathrm{int}} q_{\mathrm{int}}}{K_i}\ln\frac{r}{r_i}
  \end{align*}
  
\end{frame}

\begin{frame}
  \frametitle{General analytical solution for Poincar\'e--Steklov operator I}
  \begin{itemize}
  \item flux $q$ for specified contact resistance $R$
  \item two layers, $r_0 < r < r_{\mathrm{int}}$ and $r_{\mathrm{int}} < r < r_1$
    \begin{align*}
      \lefteqn{q=} & \\
      &\frac{T_0 - T_1 
      - \frac{s_1}{2K_1} \left(
        r_{\mathrm{int}}^2 \ln\frac{r_{\mathrm{int}}}{r_1}
        + \frac{r_1^2 - r_{\mathrm{int}}^2}{2}
      \right)
      + \frac{s_0}{2K_0}\left(
        r_{\mathrm{int}}^2 \ln\frac{r_{\mathrm{int}}}{r_0}
        + \frac{r_0^2 - r_{\mathrm{int}}^2}{2}
      \right)
                   }{
              R - r_{\mathrm{int}} 
              \left(\frac{\ln\frac{r_{\mathrm{int}}}{r_1}}{K_1}
                - \frac{\ln\frac{r_{\mathrm{int}}}{r_0}}{K_0}
              \right)}
    \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{General analytical solution for Poincar\'e--Steklov operator II}
  \begin{itemize}
  \item flux $q$ for specified temperature jump $\Delta T$
  \item two layers, $r_0 < r < r_{\mathrm{int}}$ and $r_{\mathrm{int}} < r < r_1$
  \end{itemize}
    \begin{align*}
      \lefteqn{q=} & \\
      &\frac{T_0 - T_1 - \Delta T
        - \frac{s_1}{2K_1} \left(
          r_{\mathrm{int}}^2 \ln\frac{r_{\mathrm{int}}}{r_1}
          + \frac{r_1^2 - r_{\mathrm{int}}^2}{2}
        \right)
        + \frac{s_0}{2K_0}\left(
          r_{\mathrm{int}}^2 \ln\frac{r_{\mathrm{int}}}{r_0}
          + \frac{r_0^2 - r_{\mathrm{int}}^2}{2}
      \right)
                   }{
              r_{\mathrm{int}} 
              \left(\frac{\ln\frac{r_{\mathrm{int}}}{r_1}}{K_1}
                - \frac{\ln\frac{r_{\mathrm{int}}}{r_0}}{K_0}
              \right)}
    \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Geometry and mesh}
  \begin{minipage}{.65\linewidth}
  \includegraphics[width=\linewidth]{example/disco-mesh.png}
  \end{minipage}  \begin{minipage}{.3\linewidth}
  Curved interfaces necessitate finer meshes.

  Test problem:
  \begin{itemize}
  \item $s_0 = 1, s_1=0$
  \item $K_0=1, K_1=2$
  \item $r_0=\frac{1}{2}, r_1 = 2$
  \item $r_{\mathrm{int}} = 1$
  \end{itemize}

\end{minipage}
\end{frame}

\begin{frame}[containsverbatim]
  \frametitle{Calculating mismatches}
  \begin{itemize}
  \item contact resistance $R$:
\begin{verbatim}
   v0 = u0 - u1;		// temperature jump
   real[int] q0 = B * v0[];
   v0 = R * q;		// resistive jump
   real[int] q1 = B * v0[];
   q[] = q0 - q1;
   return q[];
\end{verbatim}
  \item jump $\Delta T$:
\begin{verbatim}
   v0 = u0 - (u1 + DT);		// temperature jump shortfall
   real[int] q0 = B * v0[];
   return q0;
\end{verbatim}

  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Results for various contact resistances}
  \centering\includegraphics[width=.8\linewidth]{example/disco.png}
\end{frame}

\begin{frame}
  \frametitle{Results for various temperature jumps}
  \centering\includegraphics[width=.8\linewidth]{step/disco.png}
\end{frame}

\section{Conclusion}

\begin{frame}
  \frametitle{Conclusion}
  \begin{itemize}
  \item Many kinds of interfacial discontinuity can be handled easily
    in FreeFem++.
  \item Use domain decomposition.
  \item Guess the flux across the interface (Neumann--Neumann).
  \item Compute the mismatch (Neumann$\rightarrow$Dirichlet
    Poincar\'e--Steklov).
  \item Iterate to find the flux giving the right mismatch.
  \end{itemize}
\end{frame}

\end{document}

\date[December 2012]{Paris\\ 6 December 2012}
