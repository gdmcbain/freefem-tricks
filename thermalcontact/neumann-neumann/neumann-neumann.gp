set terminal postscript enhanced eps
set output 'neumann-neumann.eps'
mask(x,y) = y ? x : 1/0
R = 0.1				# TODO: read from neumann-neumann-R.txt
gam = (2. - R) / (2. + R)
leftexact(x) = mask((1.+x)*(gam+x)/2.,x<0)
rightexact(x) = mask(1.+(1.-x)*(x-gam)/2., x>0)

plot 'neumann-neumann.txt' u (mask($1,$1<0)):2 t 'u1',\
     '' u (mask($1,$1>0)):3 t 'u2',\
     leftexact(x) t 'exact'     ,\
     rightexact (x) not     
