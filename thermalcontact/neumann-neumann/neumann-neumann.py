#!/usr/bin/env python

# :author: G. D. McBain <gdmcbain@freeshell.org>

# :created: 2012-12-04

from sys import argv

import numpy as np

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.text import Text

def gamma(R):
    return (2. - R) / (2. + R)

def exact_left(x, R):
    return (1. + x) * (gamma(R) + x) / 2.

def exact_right(x):
    return 1. + (1. - x) * (x - gamma(R)) / 2.

fig = Figure()
canvas = FigureCanvas(fig)
ax = fig.add_subplot(111)

ax.set_xlabel(r'Position, $x$')
ax.set_ylabel(r'Exact Solution Temperature, $u$')
ax.set_ylim((-0.5, 1.5))

for R, h in zip(('100', '10', '1', '0.1', '0'),
                ('#489651', '#57c5a4', '#45c2da', '#5495d6', '#3666b9')):
    data = np.loadtxt(argv[0][:-3] + R + '.txt')
    x = data[:,0]
    T_left = data[:,1]
    T_right = data[:,2]

    ax.plot(x[x<0], T_left[x<0], '-', color=h)
    ax.plot(x[x>0], T_right[x>0], '-', color=h, label=R)
    ax.text(0, T_right[0], '{0}'.format(R), ha='right')
    # ax.plot(x[x<0], exact_left(x[x<0]), '--', label='exact')
    # ax.plot(x[x>0], exact_right(x[x>0]), '--')
        
canvas.print_figure(argv[0][:-3])
