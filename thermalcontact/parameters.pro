// parameters.pro -*- c++ -*-

// copied and pasted from https://geuz.org/trac/getdp/wiki/Capacitor2D
// 2010-05-25

//length l
//height h
//excentricity e= distance from center
//radius r

l=2.5;
h=1.;
e=-0.2;
r=0.15;

//meshsize at box edges
//meshsize at circle 
mb=l/25;
mr=r/15;

// The following are only for 
// nameing the physical objects
LPLUS=100;
LMINUS=101;
LCIRCLE=103;
SBOX=200;
SCIRCLE=201;
