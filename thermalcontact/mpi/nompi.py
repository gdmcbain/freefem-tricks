#!/usr/bin/env python

# :author: G. D. McBain <gdmcbain@freeshell.org>

# :created: 2012-12-04

from sys import argv

import numpy as np

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.text import Text

def exact_annulus(r, Ti, ri, rint, qint, K, s):
    return (Ti + (s*(rint**2 * np.log(r/ri) + (ri**2 - r**2)/2)/2
                  - rint * qint * np.log(r/ri)) / K)
            

def the_flux(s, T, K, r, rint, R=0):
    return ((T[0] - T[1] 
             - s[1] * (rint**2 * np.log(rint / r[1])
                       + (r[1]**2 - rint**2) / 2) / 2 / K[1]
             + s[0] * (rint**2 * np.log(rint / r[0]) 
                       + (r[0]**2 - rint**2) / 2) / 2 / K[0])  
            / (R - rint * (np.log(rint/r[1])/K[1] - np.log(rint/r[0])/K[0])))

def main():
    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(111)

    ax.set_xlabel(r'radius, $r$')
    ax.set_ylabel(r'temperature, $T$')

    data = np.loadtxt(argv[0][:-3] + '.txt')
    r = data[:,0]
    T_inner = data[:,1]
    T_outer = data[:,2]

    s = (1., 0.)
    T = (1., 0.)
    K = (1., 2.)
    a = (0.5, 1.0, 2.0)
    ax.set_xlim(a[0::2])

    q = the_flux(s, T, K, a[0::2], a[1], 1e-1)

    ax.plot(r[r<a[1]], T_inner[r<a[1]], 'ro', label='inner (FreeFem++)')
    ax.plot(r[r>a[1]], T_outer[r>a[1]], 'go', label='outer (FreeFem++)')
    ax.plot(r[r<a[1]], 
            exact_annulus(r[r<a[1]], T[0], a[0], a[1], q, K[0], s[0]), 
            'r-', label='inner (exact)')
    ax.plot(r[r>a[1]], 
            exact_annulus(r[r>a[1]], T[1], a[2], a[1], q, K[1], s[1]), 
            'g-', label='outer (exact)')

    ax.legend()

    canvas.print_figure(argv[0][:-3])

if __name__ == '__main__':
    main()
