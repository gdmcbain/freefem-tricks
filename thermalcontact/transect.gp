rint = 2.0
T (r, T1, T2) = r < rint ? T1 : T2
set xlabel 'radius'
set ylabel 'temperature'
set title 'Thermal contact in FreeFem++ with domain decomposition'
unset key

set terminal png
set output 'transect.png'
plot 'transect.txt' using 2:(T($2,$3,$4)) with linespoints
