Start with the equation of incompressibility u_{i,i} = 0 and Cauchy's
law, in the absence of body forces: S_{ij,j} = 0, where for a
Newtonian liquid S_{ij} = -p\delta_{ij} + 2\mu D_{ij}, and D_{ij} =
(u_{i,j} + u_{j,i})/2.

Take the inner product with a virtual velocity, v.

<v_i, S_{ij,j}> = 0

<v_i, -p,j\delta_{ij}> + 2 <v_i, \mu\epsilon_{ij,j}> = 0

. Pressure term:

  - p,j\delta_{ij} = p_i, so term is <v_i, -p_{,i}>

  - integrate by parts: <v_{i,i}, p> - [v_i, n_i p]

# Integrate by parts

Go back to Cauchy's law and integrate it by parts immediately.

<v_i, -S_{ij,j}> = 0

<v_{i,j}, S_{ij}> - [v_i, n_j S_{ij}] = 0

But S is symmetric so we can replace v_{i,j} with its symmetric part.

<D_{ij} [v], S_{ij} [u]> - [v_i, n_j S_{ij}] = 0

Now expand

<D_{ij} [v], -p\delta_{ij}> + <D_{ij} [v], 2\mu D_{ij} [u]> 

	- [v_i, n_j S_{ij}] = 0

<D_{kk} [v], -p> + <D [v], 2\mu D [u]> - [v_i, n_j S_{ij}] = 0

<D [v], 2\mu D [u]> - <div v, p> - [v_i, n_j S_{ij}] = 0

Also take the inner product of the continuity equation with a virtual
pressure q:

<q, div u> = 0

and subtract that from the above.

  <D [v], 2\mu D [u]> - <div v, p> - <q, div u> - [v_i, n_j S_{ij}] = 0

This is the weak formulation.

# Boundary conditions

At a plane patch of boundary with uniform unit normal n, the boundary
term involves

  n_j S_{ij} = - n_i p + 2 \mu (u_{i,j} + u_{j,i}) n_j

# Viscous dissipation

Now let u and p be the exact solution, so that div u = 0.

  <D [v], 2\mu D [u]> - <div v, p> - <q, div u> - [v_i, n_j S_{ij}] = 0

  <D [v], 2\mu D [u]> - <div v, p> - [v_i, n_j S_{ij}] = 0

and replace v by u.

  <D [u], 2\mu D [u]> - <div u, p> - [u_i, n_j S_{ij}] = 0

  <D [u], 2\mu D [u]> - [u_i, n_j S_{ij}] = 0

The boundary terms vanish on the solid walls and on the ports reduce
because n_j S_{ij} = -P_{inlet} n_i on the inlet and zero on the other
ports

  <D [u], 2\mu D [h]> + P_{inlet} [u_i, n_i]_inlet = 0

but [u_i, n_i]_inlet = -q_{inlet} so

  <D [u], 2\mu D [u]> - P_{inlet} q_{inlet} = 0

  q_{inlet} = <D [u], 2\mu D [u]> / P_{inlet}

and the conductivity coefficient is flowrate over pressure, so 

  g  = q_{inlet} / P_{inlet} = <D [u], 2\mu D [u]> / P_{inlet}^2 .

