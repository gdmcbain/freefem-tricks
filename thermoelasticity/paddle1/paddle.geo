// paddle.geo -*- c++ -*-

// Simple uniform elastic beam fixed at one end for demonstrating
// deformation due to temperature gradient.

// author: G. D. McBain <gdmcbain@freeshell.org>

// created: 2012-11-07

thickness = 0.2;		
length = 3.0;

Point(1) = {0, 0, 0};

downwards[] = Extrude {0, -thickness, 0} {Point{1};};
Physical Line(400) = {downwards[1]};

outwards[] = Extrude {length, 0, 1} {Line{1};};
Physical Surface(1000) = {outwards[1]};

// Point(2) = {0, -thickness, 0};
// Point(3) = {length, -thickness, 0};
// Point(4) = {length, 0, 0};

// Line(1) = {1, 2};
// Line(2) = {2, 3};
// Line(3) = {3, 4};
// Line(4) = {4, 1};

// Physical Line(400) = {1};
// Line Loop(6) = {4, 1, 2, 3};

// Plane Surface(7) = {6};
// Physical Surface(1000) = {7};
