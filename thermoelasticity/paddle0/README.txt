# Governing equations

The partial differential equations of thermoelasticity are given by
[Ignaczak & Ostoja-Starzewski (2009)]().  In particular, for the
linear isotropic equilibrium, their equations (1.1.57) through
(1.1.60) lead to

$$\sigma_{ij,j} + b_i = 0$$

$$\sigma_{ij} = 2\mu\epsilon_{ij} + \lambda\epsilon_{kk}\delta_{ij} -
(3\lambda+2\mu)\alpha\theta\delta_{ij}$$

$$2\epsilon_{ij} = u_{i,j} + u_{j,i}$$

# Weak formulation

Take the inner product of the equilibrium equation with a virtual
displacement $\mathbf{v}$.

$$\left\langle v_i, \sigma_{ij,j}\right\rangle + \langle \mathbf{v},
\mathbf{b}\rangle = 0$$


$$-\left\langle v_{i,j}, \sigma_{ij}\right\rangle + \left[ v_i,
\sigma_{ij}n_j\right] + \langle \mathbf{v}, \mathbf{b}\rangle = 0$$



$$-\left\langle \epsilon_{ij}[\mathbf{v}], \sigma_{ij}\right\rangle
  +\left[v_i, \sigma_{ij} n_j\right] + \langle \mathbf{v},
  \mathbf{b}\rangle = 0$$

$$-\left\langle \epsilon_{ij}[\mathbf{v}], 
		2\mu\epsilon_{ij} [\mathbf{u}]\right\rangle 
-\left\langle \epsilon_{ij}[\mathbf{v}], 
	      \lambda\epsilon_{kk} [\mathbf{u}]\delta_{ij}\right\rangle 
+\left\langle \epsilon_{ij}[\mathbf{v}], 
	      (3\lambda+2\mu)\alpha\theta\delta_{ij}\right\rangle 
  +\left[v_i, \sigma_{ij} n_j\right] 
+ \langle \mathbf{v}, \mathbf{b}\rangle 
= 0$$

$$-\left\langle \epsilon_{ij}[\mathbf{v}], 
		2\mu\epsilon_{ij} [\mathbf{u}]\right\rangle 
  -\left\langle \epsilon_{jj}[\mathbf{v}], 
  		\lambda\epsilon_{kk} [\mathbf{u}]\right\rangle 
  +\left\langle \epsilon_{jj}[\mathbf{v}], 
  		(3\lambda+2\mu)\alpha\theta\right\rangle 
  +\left[v_i, \sigma_{ij} n_j\right] 
  + \langle \mathbf{v}, \mathbf{b}\rangle 
= 0$$

$$-\left\langle \epsilon_{ij}[\mathbf{v}], 
		2\mu\epsilon_{ij} [\mathbf{u}]\right\rangle 
  -\left\langle \nabla\cdot\mathbf{v}, 
  		\lambda\nabla\cdot\mathbf{u}\right\rangle 
  +\left\langle \nabla\cdot\mathbf{v}, 
  		(3\lambda+2\mu)\alpha\theta\right\rangle 
  +\left[v_i, \sigma_{ij} n_j\right] 
  + \langle \mathbf{v}, \mathbf{b}\rangle 
= 0$$

## FreeFem++ implementation

Drawing on `example++-chapt3/lame.edp`, which implements
two-dimensional (i.e. plane strain) isothermal linear elastic
equilibrium and just adding the thermal term.

~~~{.edp}
macro epsilon(u1,u2) [dx(u1), dy(u2), (dy(u1)+dx(u2)) / sqrt2] // EOM;
macro div(u,v) (dx(u)+dy(v)) // EOM;
macro transpose(u) u'//EOM';

solve lame([u,v],[uu,vv]) = 
  int2d(Th)(2. * mu * (transpose(epsilon(u, v)) * epsilon(uu, vv))
	    + lambda * div(u, v) * div(uu, vv))
  + int2d(Th)((3 * lambda + 2 * mu) * alpha * div (uu, vv) * theta)
  - int2d (Th) (f * vv) 
  + on (4, u=0, v=0);
~~~
