// paddle.geo -*- c++ -*-

// Simple uniform elastic beam fixed at one end for demonstrating
// deformation due to temperature gradient.

// author: G. D. McBain <gdmcbain@freeshell.org>

// created: 2012-11-07

width = 1.0;
thickness[] = {0.2, 0.3};		
length = 5.0;

Point(1000) = {0, 0, -width/2};		// avoid physical ID numbers
hinge[] = Extrude {0, 0, width} {Point{1000};};
downwards[] = Extrude {0, -thickness[0], 0} {Line{hinge[1]};};
outwards[] = Extrude {length, 0, 0} {Surface{downwards[1]};};
underside[] = Extrude {0, -thickness[1], 0} {Surface{outwards[4]};};

Physical Volume(1) = {outwards[1]};
Physical Volume(2) = {underside[1]};

Physical Surface(4) = {downwards[1], underside[2]};
