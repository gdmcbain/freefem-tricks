This builds on the previous (../paddle1) by going to bimetallic strip.

The characteristic length scale for the mesh must be passed to Gmsh,
e.g. by prepending it to the Make command line: CLSCALE=0.1 make.

