#!/usr/bin/python
import numpy as np
c = 1.0
a = c/4 * 1.1
alpha0 = -3 * np.pi/180
mu = c/4 - a * np.exp (1j * alpha0)
zetacircle = lambda chi: mu + a * np.exp (1j * chi)
joukowsky = lambda zeta: zeta + c/2 + c**2/16/zeta

n = 120
z = joukowsky (zetacircle (alpha0 + (np.arange (n+1) * 2 * np.pi / n)))
for point in z:
    print point.real, point.imag

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

fig = Figure ()
canvas = FigureCanvas (fig)
ax = fig.add_subplot (111)
ax.plot (z.real, z.imag, 'o-')
ax.set_aspect ('equal')
canvas.print_figure ('joukowsky.png')
