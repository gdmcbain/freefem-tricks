function complex (a, b, z) {	# z = a + i*b
    z["real"] = a;
    z["imag"] = b;
}
function zprint (z) {
    print z["real"], "+i", z["imag"]; 
}
function zplus (a, b, c) {	# c 
    c["real"] = a["real"] + b["real"];
    c["imag"] = a["imag"] + b["imag"];
}
function zscale (z, k, w) {	# w = k * z
    w["real"] = k * z["real"];
    w["imag"] = k * z["imag"];
}
function zminus (a, b, w) {	# w = a - b
    zscale(b, -1, bm);
    zplus(a, bm, w);
}
function ztimes (a, b, w) {	# w = a * b
    w["real"] = a["real"] * b["real"] - a["imag"] * b["imag"];
    w["imag"] = a["real"] * b["imag"] + a["imag"] * b["real"];
}
function zmag (z) {		# return magnitude of z
    return z["real"]*z["real"] + z["imag"]*z["imag"];
}
function zreciprocal (z, w,   denom) {
    denom = zmag(z);
    w["real"] = z["real"] / denom; 
    w["imag"] = -z["imag"] / denom; 
}
function zdiv (a, b, w) {	# w = a / b
    zreciprocal(b, w);
    ztimes(a, w, w);
}
BEGIN {
    complex(1,0,a)
    printf "a = "; zprint(a);
    complex(0, sqrt (2), b);
    printf "b = "; zprint(b);
    zreciprocal(b, w);
    printf "w = "; zprint(w);
    complex(0,0,c);
    printf "c = "; zprint(c);
    zdiv(a, b, c);
    printf "c = "; zprint(c);
    zscale(c, 2);
    zprint(c);
}
