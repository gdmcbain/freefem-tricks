// Copyright (C) 2009, 2010, 2012 G. D. McBain -*- c++ -*-

// Save a scalar or vector FreeFem++ solution in Gmsh .msh format. The
// solution can be static or part of a sequence on a common mesh
// (e.g. for transient simulations or numerical continuation).

// Author: G. D. McBain <gdmcbain@freeshell.org>
// Created: 2009-08-13?
// Keywords: FreeFem++, finite element, postprocessing, Gmsh

// gmshelmtyp: Return the Gmsh element type number, inferred from the
// number of degrees of freedom per element.

func int gmshelmtyp (int n)
{
  int e;
  if (n == 3) {		// TRI3
    e = 2;
  } else if (n == 4) {	// TET4
    e = 4; 
  } else if (n == 6) {	// TRI6
    e = 9;
  } else if (n == 10) { // TET10
    e = 11;
  } else {
    cout << "Unknown element type, having " << n << "nodes!" << endl;
    assert (false);	// Shouldn't get here!
  }
  return e;    
}

// en: Return a one-dimensional array of permutation of the nodes of
// an element for output to Gmsh, for Gmsh element type t.

func int[int] en (int t) 
{
  int[int] ndofK = [0, 0, 3, 0, 4, 0, 0, 0, 0, 6, 0, 10];
  int[int] r(ndofK[t]);

  if (t == 2) {		// 3-node triangle
    r = [0, 1, 2];
  } else {
    if (t == 4) {	// 4-node tetrahedron
      r = [0, 1, 2, 3];
    } else {
      if (t == 9) {	// 6-node triangle
	r = [0, 1, 2, 5, 3, 4];
      } else { 
	if (t == 11) {	// 10-node tetrahedron
	  r = [0, 1, 2, 3, 4, 7, 5, 6, 9, 8];
	} else {
	  cout << "Unknown element type: " << t << "!" << endl;
	  assert (false);	// Shouldn't get here!
	}
      }
    }
  }
  return r;
}

macro writemshheader (ff) {
  ff << "$MeshFormat" << endl;
  ff << "2.1 0 8" << endl;
  ff << "$EndMeshFormat" << endl;
}//

// writemshdata: Write to ofstream ff the scalar finite element field
// u with name s at step it and time t as type "Node" for P1 or P2, or
// "Element" for P0.  (TODO: Try and detect P0 data automatically.)

macro writemshdata (ff, s, u, it, t, type) {
  ff << "$" + type + "Data" << endl
     << "1" << endl		
     << "\"" << s << "\"" << endl
     << "1" << endl		
     << t << endl		
     << "3" << endl		
     << it << endl		
     << "1" << endl		
     << u.n << endl;	
  for (int n = 0; n < u.n; n++)
    ff << n+1 << "  " << u[n] << endl;
  ff << "$End" + type + "Data" << endl; 
}//

macro writemshdatav (ff, s, u, v, w, it, t, type) {
  ff << "$" + type + "Data" << endl
     << "1" << endl		
     << "\"" << s << "\"" << endl
     << "1" << endl		
     << t << endl		
     << "3" << endl		
     << it << endl		
     << "3" << endl		
     << u.n << endl;	
  for (int n = 0; n < u.n; n++)
    ff << n+1 << "  " << u[n] << "  " << v[n] << "  " << w[n] << endl;
  ff << "$End" + type + "Data" << endl; 
}//

// writemshstep: Write to file filename the (scalar) finite element
// field u with name s at step it and time t, as type "Node" (P1) and
// "Element" (P0).

macro writemshstep (filename, s, u, it, t, type) {
  ofstream ff (filename);
  writemshheader (ff);
  writemshdata (ff, s, u, it, t, type);
}//

// writemshstepv: Write to file filename the vector finite element
// field with components u, v, and w with name s at step it and time
// t, as type "Node" (P1) and "Element" (P0).

macro writemshstepv (filename, s, u, v, w, it, t, type) {
  ofstream ff (filename);
  writemshheader (ff);
  writemshdatav (ff, s, u, v, w, it, t, type);
}//

// writemsh: Write the (scalar) finite element field u (of fespace
// type Vh) with name s (e.g. "temperature" or "concentration") to a
// file filename in Gmsh .msh format.

macro writemsh (filename, s, u, Th, Vh) {
  ofstream ff (filename);
  writemshheader (ff);

  {
    Vh[int] xh(3);		
    xh[0] = x;
    xh[1] = y;
    xh[2] = z;

    ff << "$Nodes" << endl;
    ff << Vh.ndof << endl;
    for (int n = 0; n < Vh.ndof; n++) {
      ff << n+1;		
      for (int d = 0; d < 3; d++)
	ff << "  " << xh[d][][n];
      ff << endl;
    }
    ff << "$EndNodes" << endl;
  }

  {
    int elmtyp = gmshelmtyp (Vh.ndofK);
    int[int] np = en (elmtyp);

    ff << "$Elements" << endl;
    ff << Vh.nt << endl;
    for (int e = 0; e < Vh.nt; e++) {
      ff << e+1 << "  "		
	 << elmtyp << "  "	
	 << "2  " 		
	 << Th[e].label << "  "	
	 << Th[e].region;	
      for (int n = 0; n < Vh.ndofK; n++)
	ff << "  " << Vh (e, np[n]) + 1;
      ff << endl;
    }
    ff << "$EndElements" << endl;
  }

  writemshdata (ff, s, u, 0, 0.0, "Node");
}//
